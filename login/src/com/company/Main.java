package com.company;
import java.sql.ResultSet;

import java.sql.SQLException;
import java.util.Scanner;

public class Main {

    public static Scanner in = new Scanner(System.in);

    public static void main(String[] args) throws SQLException {
        Users.createUserTable();
        commands();
    }

    public static void commands() throws SQLException {
        System.out.println("Введите команду: ");
        System.out.println("1. Авторизоваться.");
        System.out.println("2. Регистрация.");
        System.out.println("0. Закрыть программу");
        int cmd = in.nextInt();

        switch (cmd) {
            case 1 -> sign_in();
            case 2 -> sign_up();
            case 0 -> System.exit(0);
            default -> {
                System.out.println("Не парвильная команда! ВВедите команду обратно!");
                commands();
            }
        }
    }

    public static void sign_in() throws SQLException {
        System.out.println("Введите имя пользователя: ");
        String name = in.next();
        System.out.println(name);
        System.out.println("Введите Пароль: ");
        String password = in.next();

        ResultSet user = DB.select_query("SELECT name, password FROM users WHERE name = '" + name + "';");

        String check_password = "";
        String email = "";
        while (user.next()) {
            check_password = user.getString(2);
            name = user.getString(1);
        }


        if (user.getMetaData().getColumnCount() == 0) {
            System.out.println("Пользователья с таким именем нету");
        } else if (password.equals(check_password)) {
            System.out.println("Вы вошли в аккаунт!");
            Main.commands();
        } else {
            System.out.println("Логин или пароль не верны!");
            sign_in();
        }


    }

    public static void sign_up() throws SQLException {
        System.out.println("Введите ваше имя: ");
        String name = in.next();
        System.out.println("Введите E-Mail для регистрации: ");
        String email = in.next();
        if (check_email(email)) {
            System.out.println("Вводите пароль: ");
            String password = in.next();
            System.out.println("Введите пароль повторно: ");
            String re_password = in.next();
            if (password.equals(re_password)) {
                DB.insert("INSERT INTO users VALUES ('" + name + "', '" + email + "', '" + password + "')");
                System.out.println("Регистрация прошла успешно!");
            } else {
                System.out.println("Пароли не совпадают!");
                sign_up();
            }
        } else {
            System.out.println("E-Mail введен не правильно!");
            sign_up();
        }

    }

    public static boolean check_email(String email) {
        String sub = "@";
        return email.contains(sub);
    }


}
