package com.company;

import java.sql.SQLException;

public class Users {

    public static void createUserTable() throws SQLException {
        DB.setConnection();
        DB.createTable("CREATE TABLE IF NOT EXISTS users(" +
                "name VARCHAR(64), " +
                "email VARCHAR(64), " +
                "password VARCHAR(64) " +
                ")");
    }

}
